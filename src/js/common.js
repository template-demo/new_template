/*
 ***  Common
 */

// wow
new WOW({
  animateClass: 'animated'
}).init();


const $window = $(window).width();
$(function () {
  $("#js-pagetop").click(function () {
    // pagetop
    $("html,body").animate({ scrollTop: 0 }, "300");
  });

  $(".js-smooth").click(function () {
    const adjust = 0;
    const speed = 400;
    const href = $(this).attr("href");
    const target = $(href == "#" || href == "" ? "html" : href);
    const position = target.offset().top + adjust;
    $("body,html").animate({ scrollTop: position }, speed, "swing");
    return false;
  });
});

$(function () {
  // nav
  const $body = $("body");
  $("#js-navTrigger").click(function () {
    $body.toggleClass("navopen");
  });
});

$(function () {
  // header
  const $header = $(".header");
  const headerHeight = $header.height();
  $(window).on("scroll", function () {
    if ($(this).scrollTop() < headerHeight) {
      $header.removeClass("_scrolled");
    } else {
      $header.addClass("_scrolled");
    }
  });
});

$(function () {
  // faq
  const $faqTrigger = $(".js-faqTrigger");
  $faqTrigger.on("click", function () {
    $(this).parent().toggleClass('_active');
    $(this).next('.list-faq__item-answer').slideToggle();
  });
});
