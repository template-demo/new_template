"use strict";

/*
 ***  Common
 */
// wow
new WOW({
  animateClass: 'animated'
}).init();
var $window = $(window).width();
$(function () {
  $("#js-pagetop").click(function () {
    // pagetop
    $("html,body").animate({
      scrollTop: 0
    }, "300");
  });
  $(".js-smooth").click(function () {
    var adjust = 0;
    var speed = 400;
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? "html" : href);
    var position = target.offset().top + adjust;
    $("body,html").animate({
      scrollTop: position
    }, speed, "swing");
    return false;
  });
});
$(function () {
  // nav
  var $body = $("body");
  $("#js-navTrigger").click(function () {
    $body.toggleClass("navopen");
  });
});
$(function () {
  // header
  var $header = $(".header");
  var headerHeight = $header.height();
  $(window).on("scroll", function () {
    if ($(this).scrollTop() < headerHeight) {
      $header.removeClass("_scrolled");
    } else {
      $header.addClass("_scrolled");
    }
  });
});
$(function () {
  // faq
  var $faqTrigger = $(".js-faqTrigger");
  $faqTrigger.on("click", function () {
    $(this).parent().toggleClass('_active');
    $(this).next('.list-faq__item-answer').slideToggle();
  });
});