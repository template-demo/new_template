"use strict";

$(function () {
  /**
   * Top Page
   */
  // kv slider
  if ($("#js-topKeyvisualSlider").length) {
    $("#js-topKeyvisualSlider").slick({
      autoplay: true,
      fade: true,
      dots: false,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1
    });
  }
});